import numpy as np
import matplotlib.pyplot as plt 
import random

def kalman(duration, dt):
	measnoise = 0.01 #position measurement noise
	accelnoise = 0.002 # accelerometer noise

	a = np.array([[1, dt], [0, 1]]) # Transition matrix
	b = np.array([[(dt**2)/2], [dt]]) # input matrix
	c = np.array([[1, 0]]) # measurement matrix
	x = np.array([[0], [0]]) # Initial state vector 
	ProcessNoise = np.array([[0], [0]])
	xhat = x # Initial state estimate

	pnc = np.array([[dt**4/4, dt**3/2], [dt**3/2, dt**2]])
	Sz = measnoise**2 # Measurement error covariance
	Sw = accelnoise**2 * pnc #Process noise covariance
	P = Sw # Initial estimation covariance

	#Initialize arrays for plotting
	pos = [] #True position array
	poshat = [] #Estimated position array
	posmeas = [] #Measured position array
	vel = [] #True velocity array
	velhat = [] #Estimated velocity array

	time = np.arange(0, duration, dt)

	for t in time:
		#print("Time = " + str(t))
		u = 0 # Constant input acceleration of 1

		#Simulate the linear system
		ProcessNoise = accelnoise * np.array([[((dt**2)/2)*np.random.normal(0, 1)], [dt*np.random.normal(0, 1)]])
		
		x = a.dot(x) + b.dot(u) + ProcessNoise

		# Simulate the noisy measurement 
		MeasNoise = measnoise * np.random.normal(0, 1)
		y = c.dot(x) + MeasNoise

		#Extrapolate the most recent state estimate to the present time
		xhat = a.dot(xhat) + b.dot(u)

		#Form the innovation vector
		Inn = y - c.dot(xhat)

		#Compute the covariance of the Innovation
		s = c.dot(P).dot(np.transpose(c)) + Sz

		#Form the kalman gain matrix
		K = a.dot(P).dot(np.transpose(c)).dot(np.linalg.inv(s))

		# Update the state estimate
		xhat = xhat + K.dot(Inn)

		#Compute the covariance of the estimation error
		P = a.dot(P).dot(np.transpose(a)) - a.dot(P).dot(np.transpose(c)).dot(np.linalg.inv(s)).dot(c).dot(P).dot(np.transpose(a)) + Sw

		#Save some parameters for plotting later

		pos.append(x[0][0])
		posmeas.append(np.squeeze(y))
		poshat.append(xhat[0][0])
		vel.append(x[1][0])
		velhat.append(xhat[1][0])

	return pos, posmeas, poshat, vel, velhat

duration = 200
dt = 0.1

#Call the filter function
pos, posmeas, poshat, vel, velhat = kalman(duration, dt)


#Plot results
time = np.arange(0, duration, dt)

plt.plot(time, pos)
plt.plot(time, posmeas)
plt.plot(time, poshat)
plt.xlabel('Time (Sec)')
plt.ylabel('Position (Meters)')
plt.grid(b=True, which='both')
plt.title('Position (True, Measured, and Estimated)')
plt.show()

error1 = []
for i in range(len(pos)):
	error1.append(pos[i] - posmeas[i])
error2 = []
for i in range(len(pos)):
	error2.append(pos[i] - poshat[i])

plt.plot(time, error1)
plt.plot(time, error2)
plt.xlabel('Time (Sec)')
plt.ylabel('Position Error')
plt.grid(b=True, which='both')
plt.title('Position Measurement Error and Position Estimation Error')
plt.show()

plt.plot(time, vel)
plt.plot(time, velhat)
plt.xlabel('Time (Sec)')
plt.ylabel('Velocity (Meters/Second)')
plt.grid(b=True, which='both')
plt.title('Velocity (True, and Estimated)')
plt.show()

